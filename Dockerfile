FROM rust:1.40-buster AS build

COPY src /build/src/
COPY Cargo.toml /build/

WORKDIR /build/

RUN cargo build --all --release

RUN mkdir /app && \
    cp target/release/synchrotron target/release/federation_sender /app

FROM debian:buster

RUN apt-get update && \
    apt-get install -y openssl ca-certificates && \
    apt-get clean

COPY --from=build /app /app
