use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::time::SystemTime;

use tokio::sync::RwLock;

use colored::*;
use futures::FutureExt;

struct DBStatementsFactory {
    stmt1: tokio_postgres::Statement,
    stmt2: tokio_postgres::Statement,
}

#[derive(Clone, Debug, PartialEq)]
pub struct CachedRoom {
    pub room_id: String,
    pub last_access_time: u64,
    pub membership: Vec<String>,
    pub hosts: Vec<String>,
}

impl CachedRoom {
    pub fn new(room_id: String, membership: Vec<String>, hosts: Vec<String>) -> Self {
        Self {
            room_id,
            last_access_time: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
            membership,
            hosts,
        }
    }
}

type AtomicCachedRoom = Arc<RwLock<Option<CachedRoom>>>;

struct RoomCacheHolderData {
    rooms: HashMap<String, AtomicCachedRoom>,
    user_memberships_complete: HashSet<String>,
}

pub struct RoomCacheHolder {
    data: Arc<RwLock<RoomCacheHolderData>>,

    max_size: Option<usize>,

    db_cli: Arc<tokio_postgres::Client>,
    db_factory: Arc<DBStatementsFactory>,
}

impl RoomCacheHolder {
    pub async fn new(db_cli: Arc<tokio_postgres::Client>, max_size: Option<usize>) -> Self {
        let db_factory = DBStatementsFactory {
            stmt1: db_cli
                .prepare(
                    "SELECT user_id FROM room_memberships WHERE room_id = $1 AND membership = $2",
                )
                .await
                .unwrap(),
            stmt2: db_cli
                .prepare(
                    "SELECT room_id FROM room_memberships WHERE user_id = $1 AND membership = $2",
                )
                .await
                .unwrap(),
        };

        Self {
            data: Arc::new(RwLock::new(RoomCacheHolderData {
                rooms: HashMap::new(),
                user_memberships_complete: HashSet::new(),
            })),

            max_size,

            db_cli,

            db_factory: Arc::new(db_factory),
        }
    }

    pub async fn room(&self, room_id: String) -> CachedRoom {
        let atomic_room = {
            let room_opt = self.data.read().await.rooms.get(room_id.as_str()).cloned();

            if let Some(atomic_room) = room_opt {
                atomic_room
            } else {
                let atomic_room = Arc::new(RwLock::new(None));

                self.data
                    .write()
                    .await
                    .rooms
                    .insert(room_id.clone(), atomic_room.clone());

                atomic_room
            }
        };

        if let Some(r) = atomic_room.read().await.clone() {
            return r;
        }

        let rows: Vec<tokio_postgres::Row> = self
            .db_cli
            .query(&self.db_factory.stmt1, &[&room_id, &"join"])
            .await
            .unwrap();

        let members: Vec<String> = rows
            .iter()
            .map(|row| {
                let user_id: String = row.get(0);
                user_id
            })
            .collect();

        let hosts: Vec<String> = members
            .iter()
            .map(|member| {
                let mut iter = member.splitn(2, ":");
                iter.next().unwrap();
                iter.next().unwrap().to_string()
            })
            .collect();

        let r = CachedRoom::new(room_id.clone(), members, hosts);

        *(atomic_room.write().await) = Some(r.clone());

        self.check_overflow().await;

        r
    }

    pub async fn rooms_for_user(&self, user_id: String) -> Vec<CachedRoom> {
        if self
            .data
            .read()
            .await
            .user_memberships_complete
            .contains(user_id.as_str())
        {
            let user_rooms: Vec<CachedRoom> = Vec::new();

            for atomic_room in self.data.read().await.rooms.values() {
                if let Some(mut room) = atomic_room.write().await.clone() {
                    if !room.membership.contains(&user_id) {
                        continue;
                    }

                    room.last_access_time = SystemTime::now()
                        .duration_since(SystemTime::UNIX_EPOCH)
                        .unwrap()
                        .as_secs();
                }
            }

            return user_rooms;
        }

        let rows: Vec<tokio_postgres::Row> = self
            .db_cli
            .query(&self.db_factory.stmt2, &[&user_id, &"join"])
            .await
            .unwrap();

        let mut rooms: Vec<CachedRoom> = Vec::new();

        for row in rows {
            let room_id: String = row.get(0);

            let room = self.room(room_id).await;

            rooms.push(room);
        }

        self.data
            .write()
            .await
            .user_memberships_complete
            .insert(user_id);

        rooms
    }

    pub async fn invalidate_room(&self, room_id: String) -> bool {
        let mut data = self.data.write().await;

        let result = data.rooms.remove(room_id.as_str());

        if let Some(atomic_room_lock) = result {
            let atomic_room = atomic_room_lock.read().await;

            if let Some(room) = &*atomic_room {
                for user_id in room.membership.iter() {
                    data.user_memberships_complete.remove(user_id);
                }
            }

            return true;
        }

        return false;
    }

    pub async fn check_overflow(&self) {
        if self.max_size.is_none() {
            return;
        }

        if self.data.read().await.rooms.len() < self.max_size.unwrap() {
            return;
        }

        let room_ids: Vec<String> = self.data.read().await.rooms.keys().cloned().collect();

        for room_id in room_ids {
            self.invalidate_room(room_id).await;
        }
    }
}
