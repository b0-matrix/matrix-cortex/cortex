use ring::signature::{Ed25519KeyPair, KeyPair};
use serde_json::{json, Map, Value};

use failure::{Error, Fail};

use colored::*;
use std::collections::HashMap;

#[cfg(test)]
mod test {
    use crate::signing_key::SigningKey;
    use serde_json::from_str;
    use serde_json::{to_string, Value};

    #[test]
    fn sign_empty_json() {
        let key_pair = SigningKey::from_raw(
            "ed25519 1 YJDBA9Xnr2sVqXD9Vj7XVUnmFZcZrlw8Md7kMW+3XA0".to_string(),
        )
        .unwrap();

        let mut value: Value = from_str("{}").unwrap();

        let (new_value, sig) = key_pair.sign("domain".to_string(), value.clone());

        assert_eq!(
            to_string(&new_value).unwrap(),
            r#"{"signatures":{"domain":{"ed25519:1":"K8280/U9SSy9IVtjBuVeLr+HpOB4BQFWbg+UZaADMtTdGYI7Geitb76LTrr5QV/7Xg4ahLwYGYZzuHGZKM5ZAQ"}}}"#
        );

        assert_eq!(sig, "K8280/U9SSy9IVtjBuVeLr+HpOB4BQFWbg+UZaADMtTdGYI7Geitb76LTrr5QV/7Xg4ahLwYGYZzuHGZKM5ZAQ");
    }
}

#[derive(Debug, Clone, Fail)]
pub enum SigningKeyError {
    #[fail(display = "decode failed")]
    DecodeFailed,
    #[fail(display = "key rejected")]
    KeyRejected,
}

pub struct SigningKey {
    pub name: String,
    pub key_type: String,

    signing_key: Ed25519KeyPair,
}

impl SigningKey {
    pub fn from_raw(content: String) -> Result<Self, SigningKeyError> {
        let mut iter = content.splitn(3, char::is_whitespace);
        let key_type: String = iter.next().unwrap().to_string();
        let name: String = iter.next().unwrap().to_string();
        let unpadded_seed: &str = iter.next().unwrap().trim();

        let seed = &base64::decode(unpadded_seed).map_err(|_| SigningKeyError::DecodeFailed)?;

        let signing_key =
            Ed25519KeyPair::from_seed_unchecked(seed).map_err(|_| SigningKeyError::KeyRejected)?;

        Ok(SigningKey {
            name,
            key_type,
            signing_key,
        })
    }

    pub fn sign(&self, signature_name: String, mut value: Value) -> (Value, String) {
        let value_array = value.as_object_mut().unwrap();

        let mut signatures = value_array.remove("signatures").unwrap_or(json!({}));
        let unsigned = value_array.remove("unsigned");

        let sig = self
            .signing_key
            .sign(serde_json::to_string(&value).unwrap().as_bytes());
        let signature_base64 = base64::encode_config(&sig, base64::STANDARD_NO_PAD);

        let key_id = format!("{}:{}", self.key_type, self.name);

        if signatures
            .as_object()
            .unwrap()
            .get(signature_name.as_str())
            .is_none()
        {
            signatures
                .as_object_mut()
                .unwrap()
                .insert(signature_name.clone(), json!({}));
        }

        signatures
            .as_object_mut()
            .unwrap()
            .get_mut(signature_name.as_str())
            .unwrap()
            .as_object_mut()
            .unwrap()
            .insert(key_id, Value::from(signature_base64.clone()));

        value
            .as_object_mut()
            .unwrap()
            .insert("signatures".to_string(), signatures);

        if unsigned.is_some() {
            value
                .as_object_mut()
                .unwrap()
                .insert("unsigned".to_string(), unsigned.unwrap());
        }

        (value, signature_base64)
    }

    pub fn public_key(&self) -> &[u8] {
        self.signing_key.public_key().as_ref()
    }
}
