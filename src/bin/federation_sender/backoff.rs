use std::collections::HashMap;
use std::future::Future;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;
use std::time::SystemTime;

use tokio::sync::RwLock;

use crate::error::FederationError;

pub struct ServerBackoff {
    pub delay_for: Duration,
    pub ts: u64,
    pub is_down: bool,
    pub strikes: u8,
}

impl ServerBackoff {
    fn expired(&self) -> bool {
        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs()
            > (self.ts + self.delay_for.as_secs())
    }
}

pub struct Backoff {
    hosts: Arc<RwLock<HashMap<String, ServerBackoff>>>,
}

impl Backoff {
    const MAX_STRIKES: u8 = 5;
    const MAX_DELAY: Duration = Duration::from_secs(86400);
    const BACKOFF: Duration = Duration::from_secs(30);

    pub fn new() -> Self {
        let hosts = Arc::new(RwLock::new(HashMap::new()));

        Backoff { hosts }
    }

    pub async fn clear_backoff(&self, host: String) -> bool {
        self.hosts.write().await.remove(host.as_str()).is_some()
    }

    pub async fn host_is_down(&self, host: String) -> bool {
        let hosts = self.hosts.read().await;

        if let Some(host) = hosts.get(host.as_str()) {
            if host.is_down && !host.expired() {
                return true;
            }
        }

        return false;
    }

    pub async fn host_backoff_for(&self, hostname: String) -> Duration {
        let hosts = self.hosts.read().await;

        if let Some(h) = hosts.get(hostname.as_str()) {
            return h.delay_for;
        }

        Duration::from_secs(0)
    }

    pub async fn set_backoff(&self, hostname: String, err: FederationError) -> (bool, Duration) {
        let mut is_down = false;
        let mut strike = false;

        let mut backoff_for = Duration::from_secs(0);

        match err {
            FederationError::Busy(error_code, backoff_opt) => {
                if error_code == 429 {
                    backoff_for = Duration::from_millis(backoff_opt.unwrap_or(30000) + 30000);
                } else {
                    // 5xx
                    strike = true;
                }
            }
            FederationError::Connection => {
                is_down = true;
            }
            FederationError::Serde => {
                // Invalid JSON, is that a matrix endpoint?
                is_down = true;
            }
            FederationError::Timeout => {
                strike = true; // Is it though?
            }
            FederationError::Transaction(e) => {
                if e.error_code == "M_FORBIDDEN" && e.error.starts_with("Federation denied with") {
                    // Remote host rejects federation
                    is_down = true;
                } else if e.error_code == "M_UNKNOWN_ERROR" {
                    // Remote host is broken
                    is_down = true;
                } else if e.code.as_u16() == 404 || e.code.as_u16() == 500 {
                    is_down = true;
                }
            }
        }

        let mut hosts = self.hosts.write().await;

        if let Some(mut host) = hosts.get_mut(hostname.as_str()) {
            host.strikes = if strike { host.strikes + 1 } else { 0 };

            if host.strikes >= Self::MAX_STRIKES {
                is_down = true;
            }

            host.is_down = is_down;

            host.ts = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs();

            if !is_down {
                host.delay_for = Duration::from_secs(0);
            } else if host.delay_for < Self::MAX_DELAY {
                host.delay_for *= 2;

                if host.delay_for > Self::MAX_DELAY {
                    host.delay_for = Self::MAX_DELAY;
                }
            }
        } else if is_down {
            hosts.insert(
                hostname,
                ServerBackoff {
                    is_down: true,
                    strikes: 0,
                    delay_for: Duration::from_secs(900 + (rand::random::<i8>() as u64 / 4)),
                    ts: SystemTime::now()
                        .duration_since(SystemTime::UNIX_EPOCH)
                        .unwrap()
                        .as_secs(),
                },
            );
        } else if strike {
            hosts.insert(
                hostname,
                ServerBackoff {
                    is_down: false,
                    strikes: 1,
                    delay_for: Duration::from_secs(0),
                    ts: SystemTime::now()
                        .duration_since(SystemTime::UNIX_EPOCH)
                        .unwrap()
                        .as_secs(),
                },
            );
        }

        if backoff_for.as_secs() == 0 {
            if strike {
                backoff_for = Self::BACKOFF;
            }
        }

        return (is_down, backoff_for);
    }
}
