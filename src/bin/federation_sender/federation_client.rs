use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

use serde_json::{json, Value};

use url::Url;

use colored::*;

use synrep::streams::StreamRows::Federation;

use cortex::matrix::resolver::{HostRecord, HostResolver};

use crate::error::{FederationError, TransactionFailureError};
use crate::signing_key::SigningKey;
use crate::transaction::Transaction;

pub struct FederationClient {
    origin: String,
    key: SigningKey,
    resolver: HostResolver,

    http_client: reqwest::Client,
}

impl FederationClient {
    pub async fn new(server_name: String, key: SigningKey, unsafe_tls: bool) -> Self {
        let http_client = reqwest::Client::builder()
            .tcp_nodelay()
            .danger_accept_invalid_certs(unsafe_tls)
            .timeout(Duration::from_secs(60))
            .build()
            .unwrap();

        let (resolver, background) = HostResolver::new(8448).await;

        tokio::spawn(background);

        FederationClient {
            origin: server_name,
            key,
            resolver,
            http_client,
        }
    }

    pub async fn send_transaction(&self, transaction: &Transaction) -> Result<(), FederationError> {
        let record = self
            .resolver
            .host_record(transaction.destination.clone())
            .await;

        if record.is_down {
            return Err(FederationError::Connection);
        }

        self.send_transaction_with_record(transaction, record).await
    }

    async fn send_transaction_with_record(
        &self,
        transaction: &Transaction,
        record: HostRecord,
    ) -> Result<(), FederationError> {
        let mut request_url =
            url::Url::parse(format!("https://{}", record.resolved.hostname).as_str()).unwrap();

        request_url.set_scheme("https").unwrap();
        request_url
            .set_host(Some(record.resolved.hostname.to_string().as_str()))
            .unwrap();
        request_url.set_port(record.resolved.port.clone()).unwrap();
        request_url.set_path(
            format!(
                "/_matrix/federation/v1/send/{}",
                transaction.transaction_id.clone()
            )
            .as_str(),
        );

        let real_host: String = if record.srv {
            record.host.clone()
        } else {
            record.resolved.hostname.to_string()
        };

        let body = serde_json::to_value(transaction).unwrap();

        let (signed_body, matrix_header) = self.sign_request(
            "PUT".to_string(),
            request_url.clone(),
            transaction.destination.clone(),
            body.clone(),
        );

        let auth_header = format!("X-Matrix {}", matrix_header);

        let request_url = request_url.clone();
        let real_host = real_host.clone();
        let auth_header = auth_header.clone();

        let result = self
            .http_client
            .put(request_url)
            .header(reqwest::header::HOST, real_host)
            .header(reqwest::header::AUTHORIZATION, auth_header)
            .json(&body)
            .send()
            .await;

        match result {
            Ok(resp) => {
                let status_code = resp.status().clone();
                let status_code_u16 = status_code.as_u16();

                let mut is_busy = false;

                if status_code_u16 == 502 || status_code_u16 == 503 || status_code_u16 == 429 {
                    is_busy = true;
                }

                match resp.json().await {
                    Ok(resp_json) => {
                        let resp_json: Value = resp_json;

                        if resp_json.get("errcode").is_some() || !status_code.is_success() {
                            if is_busy {
                                let backoff_for = resp_json
                                    .get("retry_after_ms")
                                    .unwrap_or(&Value::Null)
                                    .as_u64();

                                return Err(FederationError::Busy(status_code_u16, backoff_for));
                            }

                            return Err(FederationError::Transaction(
                                TransactionFailureError::new(
                                    transaction.destination.clone(),
                                    status_code,
                                    resp_json,
                                ),
                            ));
                        }

                        return Ok(());
                    }
                    Err(e) => {
                        if e.is_timeout() {
                            return Err(FederationError::Timeout);
                        }

                        if is_busy {
                            return Err(FederationError::Busy(status_code_u16, None));
                        }

                        if !status_code.is_success() {
                            return Err(FederationError::Connection);
                        }

                        return Err(FederationError::Serde);
                    }
                }
            }
            Err(e) => {
                if e.is_timeout() {
                    return Err(FederationError::Timeout);
                }

                return Err(FederationError::Connection);
            }
        };

        Ok(())
    }

    fn sign_request(
        &self,
        method: String,
        url: Url,
        destination: String,
        body: Value,
    ) -> (Value, String) {
        let mut signing_body: Value = json!({
        "destination": destination,
        "method": method.to_uppercase(),
        "origin": self.origin.clone(),
        "uri": url[url::Position::BeforePath..].to_string(),
        });

        if !body.is_null() {
            signing_body
                .as_object_mut()
                .unwrap()
                .insert("content".to_string(), body);
        }

        let (signed_json, sig) = self.key.sign(self.origin.clone(), signing_body.clone());

        (
            signed_json,
            format!(
                r#"origin={},key="{}:{}",sig="{}""#,
                self.origin, self.key.key_type, self.key.name, sig
            ),
        )
    }

    pub async fn clear_backoff(&self, server_name: String) {
        self.resolver.clear_record(server_name).await;
    }
}
