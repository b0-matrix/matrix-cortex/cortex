use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json;
use serde_json::Value;

use synrep::edu_event::EduEvent;

#[derive(Serialize, Clone)]
pub struct Transaction {
    #[serde(skip_serializing)]
    pub transaction_id: String,
    #[serde(skip_serializing)]
    pub destination: String,
    pub origin: String,
    pub origin_server_ts: u64,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub previous_ids: Vec<String>,

    pub edus: Vec<EduEvent>,
    pub pdus: Vec<PduEvent>,
}

#[derive(Serialize, Clone)]
#[serde(untagged)]
pub enum PduEvent {
    V1(PduEventV1),
    V2(PduEventV2),
}

#[derive(Serialize, Clone)]
pub struct PduEventV1 {
    pub event_id: String,
    pub room_id: String,
    pub sender: String,
    pub origin: String,
    pub origin_server_ts: u64,
    #[serde(rename = "type")]
    pub event_type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,
    pub content: Value,
    pub prev_events: Value,
    pub depth: u64,
    pub auth_events: Value,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<String>,
    pub unsigned: Option<Value>,
    pub hashes: Value,
    pub signatures: HashMap<String, HashMap<String, String>>,
    pub prev_state: Option<Value>,
}

#[derive(Serialize, Clone)]
pub struct PduEventV2 {
    pub room_id: String,
    pub sender: String,
    pub origin: String,
    pub origin_server_ts: u64,
    #[serde(rename = "type")]
    pub event_type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<String>,
    pub unsigned: Option<Value>,
    pub prev_events: Value,
    pub auth_events: Value,
    pub depth: u64,
    pub hashes: Value,
    pub signatures: HashMap<String, HashMap<String, String>>,
    pub content: Value,
    pub prev_state: Option<Value>,
}

pub struct PduEventHash {
    pub sha256: String,
}
