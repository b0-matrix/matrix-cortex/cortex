use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use tokio::sync::Mutex;
use tokio::sync::RwLock;

use serde_json::{json, Value};

use colored::*;

use synrep::edu_event::EduEvent;
use synrep::presence_state::PresenceState;

use cortex::matrix::model::event::Event;
use cortex::util::room_cache::RoomCacheHolder;

use crate::backoff::Backoff;
use crate::device_content_set::DeviceContentSet;
use crate::error::FederationError;
use crate::federation_client::FederationClient;
use crate::signing_key::SigningKey;
use crate::transaction::{PduEvent, PduEventV1, PduEventV2, Transaction};
use cortex::db::model::device_federation_outbox::DeviceFederationOutbox;
use cortex::db::model::device_lists_outbound_pokes::DeviceListsOutboundPokes;
use cortex::db::model::devices::Devices;

type AtomicTransaction = Arc<RwLock<Transaction>>;

struct DBStatementsFactory {
    device_messages_for_destination_1: tokio_postgres::Statement,
    device_messages_for_destination_2: tokio_postgres::Statement,
    clear_device_messages_1: tokio_postgres::Statement,
    clear_device_messages_2: tokio_postgres::Statement,
    clear_device_messages_3: tokio_postgres::Statement,
    clear_device_messages_4: tokio_postgres::Statement,
    process_pending_events_1: tokio_postgres::Statement,
    process_pending_events_2: tokio_postgres::Statement,
    process_pending_events_3: tokio_postgres::Statement,
}

struct TransactionQueueData {
    txn_id: u64,

    dest_last_device_list_stream_id: HashMap<String, u64>,
    dest_last_device_msg_stream_id: HashMap<String, u64>,
    dest_pending_transactions: HashMap<String, Vec<AtomicTransaction>>,
    dest_ongoing_transactions: HashMap<String, bool>,
    user_presence: HashMap<String, PresenceState>,
    device_list_update_destinations: HashSet<String>,

    is_processing_events: bool,
    is_processing_presence: bool,
    is_processing_device_list_updates: bool,
    last_event_poke: u64,
}

pub struct TransactionQueue {
    server_name: String,

    max_parallel_request: Option<usize>,
    max_hosts_for_presence: Option<usize>,

    data: Arc<RwLock<TransactionQueueData>>,

    backoff: Arc<Backoff>,
    fed_cli: Arc<FederationClient>,
    db_cli: Arc<tokio_postgres::Client>,
    room_cache_holder: RoomCacheHolder,
    db_factory: Arc<DBStatementsFactory>,
}

impl TransactionQueue {
    const MAX_PDUS_PER_TRANSACTION: usize = 50;
    const MAX_EDUS_PER_TRANSACTION: usize = 100;

    pub async fn new(
        server_name: String,
        db_cli: Arc<tokio_postgres::Client>,
        signing_key: SigningKey,
        max_parallel_request: Option<usize>,
        max_hosts_for_presence: Option<usize>,
        max_room_cache: Option<usize>,
    ) -> Self {
        // Preparing database
        let stmt11 = db_cli
            .prepare("DROP FUNCTION IF EXISTS cortex_fetch_devices_for_user")
            .await
            .unwrap();
        db_cli.execute(&stmt11, &[]).await.unwrap();
        let stmt12 = db_cli
            .prepare(
                "CREATE OR REPLACE FUNCTION cortex_fetch_devices_for_user (
    dest TEXT,
    devices_limit INTEGER)
    RETURNS TABLE (
        deleted BOOLEAN,
        user_id TEXT,
        device_id TEXT,
        stream_id BIGINT,
        display_name TEXT,
        key_json TEXT,
        last_successful_stream_id BIGINT)

AS
$$

DECLARE
    i RECORD;
BEGIN
FOR i IN SELECT dlop.user_id, dlop.device_id, dlop.stream_id
    FROM device_lists_outbound_pokes dlop
    WHERE dlop.destination = dest AND dlop.sent = false
    ORDER BY stream_id
    LIMIT devices_limit
LOOP
deleted := false;
user_id := i.user_id;
device_id := i.device_id;
stream_id := i.stream_id;
BEGIN
SELECT d.display_name, edkj.key_json
    INTO STRICT display_name, key_json
    FROM devices d,
         e2e_device_keys_json edkj
    WHERE d.user_id = i.user_id
      AND d.device_id = i.device_id
      AND edkj.user_id = i.user_id
      AND edkj.device_id = i.device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            deleted := true;
END;
SELECT dlols.stream_id
    INTO last_successful_stream_id
    FROM device_lists_outbound_last_success dlols
    WHERE dlols.destination = dest
      AND dlols.user_id = i.user_id
      AND dlols.stream_id < i.stream_id
    ORDER BY stream_id DESC
    LIMIT 1;
RETURN NEXT;
END LOOP;
END;
$$ LANGUAGE plpgsql;",
            )
            .await
            .unwrap(); // Create function for fetching device list

        db_cli.execute(&stmt12, &[]).await.unwrap();

        let room_cache_holder = RoomCacheHolder::new(db_cli.clone(), max_room_cache).await;

        let db_factory = DBStatementsFactory {
            device_messages_for_destination_1: db_cli.prepare("SELECT destination,stream_id,queued_ts,messages_json FROM device_federation_outbox WHERE destination = $1 AND stream_id > $2 ORDER BY stream_id LIMIT $3").await.unwrap(),
            device_messages_for_destination_2: db_cli.prepare("SELECT deleted, user_id, device_id, stream_id, display_name, key_json, last_successful_stream_id FROM cortex_fetch_devices_for_user($1, $2)").await.unwrap(),
            clear_device_messages_1: db_cli.prepare("DELETE FROM device_federation_outbox WHERE destination = $1 AND stream_id = ANY($2)").await.unwrap(),
            clear_device_messages_2: db_cli.prepare("UPDATE device_lists_outbound_pokes SET sent = true WHERE destination = $1 AND user_id = $2 AND device_id = $3 AND stream_id = $4 AND sent = false").await.unwrap(),
            clear_device_messages_3: db_cli.prepare("UPDATE device_lists_outbound_last_success SET stream_id = $3 WHERE stream_id = (SELECT stream_id FROM device_lists_outbound_last_success WHERE stream_id < $3 AND destination = $1 AND user_id = $2 LIMIT 1) AND destination = $1 AND user_id = $2").await.unwrap(),
            clear_device_messages_4: db_cli.prepare("INSERT INTO device_lists_outbound_last_success (destination, user_id, stream_id) VALUES ($1, $2, $3)").await.unwrap(),
            process_pending_events_1: db_cli
                .prepare("SELECT stream_id FROM federation_stream_position WHERE type = $1")
                .await
                .unwrap(),
            process_pending_events_2: db_cli.prepare("SELECT e.event_id,e.stream_ordering,e.type,e.room_id,e.sender,j.json,j.format_version FROM events e,event_json j WHERE e.event_id = j.event_id AND e.stream_ordering > $1 AND e.stream_ordering <= $2 ORDER BY e.stream_ordering ASC LIMIT $3").await.unwrap(),
            process_pending_events_3: db_cli.prepare("UPDATE federation_stream_position SET stream_id = $1 WHERE type = $2").await.unwrap(),
        };

        let data = TransactionQueueData {
            dest_last_device_list_stream_id: HashMap::new(),
            dest_last_device_msg_stream_id: HashMap::new(),
            dest_pending_transactions: HashMap::new(),
            dest_ongoing_transactions: HashMap::new(),
            user_presence: HashMap::new(),
            device_list_update_destinations: HashSet::new(),
            txn_id: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
            is_processing_events: false,
            is_processing_presence: false,
            is_processing_device_list_updates: false,
            last_event_poke: 0,
        };

        let fed_cli = FederationClient::new(server_name.clone(), signing_key, true).await;
        let backoff = Backoff::new();

        TransactionQueue {
            data: Arc::new(RwLock::new(data)),

            max_parallel_request,
            max_hosts_for_presence,

            fed_cli: Arc::new(fed_cli),
            db_cli,

            server_name,

            backoff: Arc::new(backoff),

            room_cache_holder,

            db_factory: Arc::new(db_factory),
        }
    }

    pub async fn send_presence(&self, presence_vec: Vec<PresenceState>) {
        {
            let mut data = self.data.write().await;

            for presence in presence_vec.iter() {
                let mut iter = presence.user_id.splitn(2, ":");
                iter.next().unwrap();
                let sender_server: String = iter.next().unwrap().to_string();

                if sender_server != self.server_name {
                    continue;
                }

                println!(
                    "{:<16} | new presence {} = {}@{}",
                    "send_presence".cyan().bold(),
                    presence.user_id,
                    presence.state,
                    presence.last_active_ts
                );

                data.user_presence
                    .insert(presence.user_id.clone(), presence.clone());
            }

            if data.is_processing_presence {
                return;
            }
        }

        self.data.write().await.is_processing_presence = true;

        self.process_pending_presence().await;

        self.data.write().await.is_processing_presence = false;
    }

    async fn process_pending_presence(&self) {
        let presence_vec = {
            let mut unlocked_data = self.data.write().await;
            let mut presence_vec: Vec<PresenceState> = Vec::new();

            for (user_id, presence) in unlocked_data.user_presence.iter() {
                presence_vec.push(presence.clone());
            }

            unlocked_data.user_presence.clear();

            presence_vec
        };

        let hosts_and_state = self.presence_destinations(presence_vec).await;

        let mut i = 0;
        let mut edu = 0;

        for (hosts, state) in hosts_and_state.iter() {
            i += 1;

            println!(
                "{:<16} | processing {}/{}",
                "proc_presence".cyan().bold(),
                i,
                hosts_and_state.len()
            );

            let now = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_millis();

            let mut presence_event_json: Value = json!({
                "presence": state.state.clone(),
                "user_id": state.user_id.clone(),
            });

            let mut presence_event_json_obj = presence_event_json.as_object_mut().unwrap();

            if state.last_active_ts != 0 {
                presence_event_json_obj.insert(
                    "last_active_ago".to_string(),
                    json!(now as u64 - state.last_active_ts),
                );
            }

            if let Some(msg) = state.status_msg.clone() {
                if state.state != "offline" {
                    presence_event_json_obj.insert("status_msg".to_string(), json!(msg));
                }
            }

            if state.state == "online" {
                presence_event_json_obj.insert(
                    "currently_active".to_string(),
                    json!(state.currently_active),
                );
            }

            for host in hosts {
                let atomic_transaction = self.transaction_for_destination(host.clone()).await;

                let mut transaction = atomic_transaction.write().await;

                transaction.edus.push(EduEvent {
                    destination: host.clone(),
                    origin: self.server_name.clone(),
                    edu_type: "m.presence".to_string(),
                    internal_key: "".to_string(),
                    content: presence_event_json.clone(),
                    stream_id: 0,
                });

                edu += 1;
            }
        }

        for (hosts, _) in hosts_and_state {
            for destination in hosts {
                tokio::spawn(Self::send_transactions_for_destination_static(
                    destination,
                    self.data.clone(),
                    self.fed_cli.clone(),
                    self.db_cli.clone(),
                    self.backoff.clone(),
                    self.db_factory.clone(),
                ));
            }
        }
    }

    pub async fn send_edu(&self, ev: EduEvent) {
        let destination = ev.destination.clone();

        if destination == self.server_name {
            return;
        }

        if self.backoff.host_is_down(destination.clone()).await {
            return;
        }

        if !self
            .data
            .read()
            .await
            .dest_last_device_msg_stream_id
            .contains_key(&destination)
        {
            self.send_device_messages(destination.clone()).await;
        }

        let atomic_transaction = self.transaction_for_destination(destination.clone()).await;

        atomic_transaction.write().await.edus.push(ev);

        //        println!(
        //            "{:<16} | sending edus to {}",
        //            "send_edu".cyan().bold(),
        //            destination
        //        );

        let data = self.data.clone();
        let fed_cli = self.fed_cli.clone();
        let db_cli = self.db_cli.clone();
        let backoff = self.backoff.clone();
        let db_factory = self.db_factory.clone();

        tokio::spawn(async move {
            Self::send_transactions_for_destination_static(
                destination,
                data,
                fed_cli,
                db_cli,
                backoff,
                db_factory,
            )
                .await;
        });
    }

    pub async fn send_keyed_edu(&self, mut ev: EduEvent, key: String) {
        let destination = ev.destination.clone();

        if destination == self.server_name {
            return;
        }

        if self.backoff.host_is_down(destination.clone()).await {
            return;
        }

        {
            let data = self.data.read().await;
        }

        {
            let atomic_transaction = self.transaction_for_destination(destination.clone()).await;
            let mut transaction = atomic_transaction.write().await;

            transaction.edus.retain(|edu| edu.internal_key != key);
        }

        ev.internal_key = key;

        self.send_edu(ev).await;
    }

    pub async fn send_device_messages(&self, destination: String) {
        if self.server_name == destination {
            return;
        }

        if self.backoff.host_is_down(destination.clone()).await {
            println!(
                "{:<16} | not sending device messages to {}: host is down",
                "send_dev_msg".cyan().bold(),
                destination
            );
        }

        let (messages, device_content_sets) = self
            .device_messages_for_destination(destination.clone())
            .await;

        if messages.is_empty() && device_content_sets.is_empty() {
            return;
        }

        println!(
            "{:<16} | sending device messages to {} msg:{}, list:{}",
            "send_dev_msg".cyan().bold(),
            destination,
            messages.len(),
            device_content_sets.len(),
        );

        for message in messages {
            let atomic_transaction = self.transaction_for_destination(destination.clone()).await;
            let mut transaction = atomic_transaction.write().await;

            let edu = EduEvent {
                content: message.message_json,
                edu_type: "m.direct_to_device".to_string(),
                internal_key: "".to_string(),
                stream_id: message.stream_id,
                origin: self.server_name.clone(),
                destination: destination.clone(),
            };

            transaction.edus.push(edu);
        }

        for dcs in device_content_sets {
            let atomic_transaction = self.transaction_for_destination(destination.clone()).await;
            let mut transaction = atomic_transaction.write().await;

            let edu = EduEvent {
                content: serde_json::to_value(&dcs).unwrap(),
                edu_type: "m.device_list_update".to_string(),
                internal_key: "".to_string(),
                stream_id: dcs.stream_id,
                origin: self.server_name.clone(),
                destination: destination.clone(),
            };

            transaction.edus.push(edu);
        }

        Self::send_transactions_for_destination_static(
            destination.clone(),
            self.data.clone(),
            self.fed_cli.clone(),
            self.db_cli.clone(),
            self.backoff.clone(),
            self.db_factory.clone(),
        )
            .await;
    }

    async fn device_messages_for_destination(
        &self,
        destination: String,
    ) -> (Vec<DeviceFederationOutbox>, Vec<DeviceContentSet>) {
        let data = self.data.read().await;

        let last_msg_id = data
            .dest_last_device_msg_stream_id
            .get(&destination)
            .unwrap_or(&0)
            .clone();
        let last_list_id = data
            .dest_last_device_list_stream_id
            .get(&destination)
            .unwrap_or(&0)
            .clone();

        let rows: Vec<tokio_postgres::Row> = self
            .db_cli
            .query(
                &self.db_factory.device_messages_for_destination_1,
                &[
                    &destination,
                    &(last_msg_id as i64),
                    &(Self::MAX_EDUS_PER_TRANSACTION as i64),
                ],
            )
            .await
            .unwrap();

        let mut messages: Vec<DeviceFederationOutbox> = Vec::new();

        for row in rows {
            let message = DeviceFederationOutbox {
                destination: row.get(0),
                stream_id: row.get::<usize, i64>(1) as u64,
                queued_ts: row.get::<usize, i64>(2) as u64,
                message_json: serde_json::from_str(row.get::<usize, String>(3).as_str()).unwrap(),
            };

            messages.push(message);
        }

        let rows: Vec<tokio_postgres::Row> = self
            .db_cli
            .query(
                &self.db_factory.device_messages_for_destination_2,
                &[&destination, &(Self::MAX_EDUS_PER_TRANSACTION as i32)],
            )
            .await
            .unwrap();

        let mut device_content_sets: Vec<DeviceContentSet> = Vec::new();

        for row in rows {
            let deleted: bool = row.get(0);

            let user_id: String = row.get(1);
            let device_id: String = row.get(2);
            let stream_id = row.get::<usize, i64>(3) as u64;

            let display_name: Option<String> = row.get(4);
            let key_json_string: Option<String> = row.get(5);
            let prev_id: Option<i64> = row.get(6);

            let device: Option<Devices> = {
                if !deleted {
                    let device = Devices {
                        user_id: user_id.clone(),
                        device_id: device_id.clone(),
                        display_name: display_name.clone(), // Might be null
                    };

                    Some(device)
                } else {
                    None
                }
            };

            let mut prev_ids: Vec<u64> = {
                if let Some(pid) = prev_id {
                    vec![pid as u64]
                } else {
                    Vec::new()
                }
            };

            let key_json: serde_json::Value = {
                if let Some(kjs) = key_json_string {
                    serde_json::from_str(kjs.as_str()).unwrap()
                } else {
                    serde_json::Value::Null
                }
            };

            let dcs = DeviceContentSet {
                deleted,
                device_display_name: display_name,
                device_id,
                keys: key_json,
                prev_id: prev_ids,
                stream_id,
                user_id,
            };

            device_content_sets.push(dcs);
        }

        (messages, device_content_sets)
    }

    async fn clear_device_messages(
        transaction: &Transaction,
        data: Arc<RwLock<TransactionQueueData>>,
        db_cli: Arc<tokio_postgres::Client>,
        db_factory: Arc<DBStatementsFactory>,
    ) {
        let destination = transaction.destination.clone();

        let (device_msgs, device_lists) = {
            let device_msgs: Vec<u64> = transaction
                .edus
                .iter()
                .filter(|e| e.edu_type == "m.direct_to_device")
                .map(|e| e.stream_id)
                .collect();
            let device_lists: Vec<EduEvent> = transaction
                .edus
                .iter()
                .filter(|e| e.edu_type == "m.device_list_update")
                .cloned()
                .collect();

            if device_msgs.is_empty() && device_lists.is_empty() {
                return;
            }

            (device_msgs, device_lists)
        };

        if !device_msgs.is_empty() {
            {
                let mut data = data.write().await;

                data.dest_last_device_msg_stream_id.insert(
                    destination.clone(),
                    device_msgs.iter().max().unwrap_or(&0).clone(),
                );
            }

            let rows_deleted = db_cli
                .execute(
                    &db_factory.clear_device_messages_1,
                    &[
                        &destination,
                        &device_msgs
                            .iter()
                            .map(|i| i.clone() as i64)
                            .collect::<Vec<i64>>(),
                    ],
                )
                .await
                .unwrap();

            if rows_deleted < 1 {
                println!("no messages to delete in outbox, despite sending messages in this txn")
            }
        }

        if device_lists.is_empty() {
            return;
        }

        {
            let mut data = data.write().await;

            data.dest_last_device_list_stream_id.insert(
                destination.clone(),
                device_lists.iter().map(|e| e.stream_id).max().unwrap_or(0),
            );
        }

        for list_ev in device_lists {
            let content_set = list_ev.content;

            let user_id: String = content_set["user_id"].as_str().unwrap().to_string();
            let device_id: String = content_set["device_id"].as_str().unwrap().to_string();
            let stream_id: u64 = content_set["stream_id"].as_u64().unwrap();

            db_cli
                .execute(
                    &db_factory.clear_device_messages_2,
                    &[&destination, &user_id, &device_id, &(stream_id as i64)],
                )
                .await
                .unwrap();

            let altered_count = db_cli
                .execute(
                    &db_factory.clear_device_messages_3,
                    &[&destination, &user_id, &(stream_id as i64)],
                )
                .await
                .unwrap();
            if altered_count < 1 {
                db_cli
                    .execute(
                        &db_factory.clear_device_messages_4,
                        &[&destination, &user_id, &(stream_id as i64)],
                    )
                    .await
                    .unwrap();
            }
        }
    }

    pub async fn set_event_position(&self, stream_position: u64) {
        {
            let mut data = self.data.write().await;

            let last_event_poke = data.last_event_poke;
            data.last_event_poke = std::cmp::max(last_event_poke, stream_position);

            if data.is_processing_events {
                //                println!(
                //                    "{:<16} | {}",
                //                    "set_event_pos".cyan().bold(),
                //                    "processing last event change, ignoring"
                //                );

                return;
            }
        }

        self.data.write().await.is_processing_events = true;

        self.process_all_pending_events().await;

        self.data.write().await.is_processing_events = false;
    }

    pub async fn clear_backoff(&self, server_name: String) {
        self.backoff.clear_backoff(server_name.clone()).await;
        self.fed_cli.clear_backoff(server_name).await;
    }

    async fn process_all_pending_events(&self) {
        loop {
            let result = self.process_pending_events().await;

            if result {
                //                println!(
                //                    "{:<16} | {}",
                //                    "proc_pend_ev".cyan().bold(),
                //                    "all pending events scheduled for sending"
                //                );

                return;
            }
        }
    }

    async fn process_pending_events(&self) -> bool {
        let last_event_poke = self.data.read().await.last_event_poke;

        let start_time = SystemTime::now();

        let mut top = last_event_poke as i32;

        let rows: Vec<tokio_postgres::Row> = self
            .db_cli
            .query(&self.db_factory.process_pending_events_1, &[&"events"])
            .await
            .unwrap();

        let federation_out_pos: i32 = rows[0].get(0);

        let time1 = SystemTime::now()
            .duration_since(start_time.clone())
            .unwrap()
            .as_millis();

        let rows: Vec<tokio_postgres::Row> = self
            .db_cli
            .query(
                &self.db_factory.process_pending_events_2,
                &[
                    &federation_out_pos,
                    &top,
                    &(Self::MAX_PDUS_PER_TRANSACTION as i64),
                ],
            )
            .await
            .unwrap();

        let time2 = SystemTime::now()
            .duration_since(start_time.clone())
            .unwrap()
            .as_millis();

        if rows.is_empty() {
            return true;
        }

        if rows.len() == Self::MAX_PDUS_PER_TRANSACTION {
            top = rows.last().unwrap().get(1);
        }

        let events: Vec<Event> = rows
            .iter()
            .map(|row| {
                let event_id: String = row.get(0);
                let event_type: String = row.get(2);
                let room_id: String = row.get(3);
                let sender_id: String = row.get(4);
                let content_string: String = row.get(5);
                let content: serde_json::Value =
                    serde_json::from_str(content_string.as_str()).unwrap();
                let format_version: u32 = {
                    let res: i32 = row.get(6);
                    res as u32
                };

                Event {
                    event_id,
                    event_type,
                    room_id,
                    sender_id,
                    content,
                    format_version,
                }
            })
            .collect();

        let mut hosts_to_send_to: HashSet<String> = HashSet::new();

        let mut grouped_events: HashMap<String, Vec<Event>> = HashMap::new();

        for event in events.iter() {
            if event.event_type == "m.room.member" {
                self.room_cache_holder
                    .invalidate_room(event.room_id.clone())
                    .await;
            }
        }

        for event in events.iter() {
            let mut iter = event.sender_id.splitn(2, ":");
            iter.next().unwrap();
            let sender_server = iter.next().unwrap().to_string();

            if sender_server != self.server_name {
                continue;
            }

            if !grouped_events.contains_key(event.room_id.as_str()) {
                grouped_events.insert(event.room_id.clone(), Vec::new());
            }

            if let Some(list) = grouped_events.get_mut(event.room_id.as_str()) {
                list.push(event.clone());
            }
        }

        for (room_id, room_events) in grouped_events.iter() {
            let unfiltered_hosts = self.room_cache_holder.room(room_id.clone()).await.hosts;

            let mut filtered_hosts = Vec::new();

            for host in unfiltered_hosts.iter() {
                if host.clone() == self.server_name {
                    continue;
                }

                if self.backoff.host_is_down(host.clone()).await {
                    continue;
                }

                filtered_hosts.push(host.clone());
            }

            let mut hosts: HashSet<String> = HashSet::from_iter(filtered_hosts.iter().cloned());

            if hosts.len() == 0 {
                continue;
            }

            hosts_to_send_to.extend(hosts.clone());

            for room_event in room_events {
                let content = room_event.content["content"].clone();
                let depth = room_event.content["depth"].as_u64().unwrap();
                let auth_events = room_event.content["auth_events"].clone();
                let prev_events = room_event.content["prev_events"].clone();
                let origin_server_ts = room_event.content["origin_server_ts"].as_u64().unwrap();
                let redacts = room_event
                    .content
                    .get("redacts")
                    .map(|r| r.as_str().unwrap().to_string());
                let prev_state = room_event.content.get("prev_state").cloned();
                let state_key = room_event
                    .content
                    .get("state_key")
                    .map(|r| r.as_str().unwrap().to_string());
                let event_type = room_event.content["type"].as_str().unwrap().to_string();
                let unsigned = room_event.content.get("unsigned").cloned();
                let hashes = room_event.content["hashes"].clone();

                let signatures: HashMap<String, HashMap<String, String>> =
                    serde_json::from_value(room_event.content["signatures"].clone()).unwrap();

                let pdu_event: PduEvent = if room_event.format_version == 1 {
                    PduEvent::V1(PduEventV1 {
                        event_id: room_event.event_id.clone(),
                        room_id: room_id.clone(),
                        sender: room_event.sender_id.clone(),
                        origin: self.server_name.clone(),
                        origin_server_ts,
                        event_type: event_type.clone(),
                        state_key: state_key.clone(),
                        content,
                        prev_events,
                        depth,
                        auth_events,
                        redacts,
                        unsigned,
                        hashes,
                        signatures,
                        prev_state,
                    })
                } else {
                    PduEvent::V2(PduEventV2 {
                        room_id: room_id.clone(),
                        sender: room_event.sender_id.clone(),
                        origin: self.server_name.clone(),
                        origin_server_ts,
                        event_type: event_type.clone(),
                        state_key: state_key.clone(),
                        content,
                        prev_events,
                        depth,
                        auth_events,
                        redacts,
                        unsigned,
                        hashes,
                        signatures,
                        prev_state,
                    })
                };

                let mut hosts_to_send: HashSet<String> = HashSet::from_iter(hosts.iter().cloned());

                if event_type == "m.room.member" && state_key.is_some() {
                    let state_key = state_key.unwrap();
                    let mut iter = state_key.splitn(2, ":");
                    iter.next().unwrap();
                    let dest_server: String = iter.next().unwrap().to_string();
                    hosts_to_send.insert(dest_server);
                }

                for host in hosts_to_send {
                    let atomic_transaction = self.transaction_for_destination(host.clone()).await;
                    let mut transaction = atomic_transaction.write().await;

                    transaction.pdus.push(pdu_event.clone());
                }
            }
        }

        let time3 = SystemTime::now()
            .duration_since(start_time.clone())
            .unwrap()
            .as_millis();

        for destination in hosts_to_send_to {
            tokio::spawn(Self::send_transactions_for_destination_static(
                destination,
                self.data.clone(),
                self.fed_cli.clone(),
                self.db_cli.clone(),
                self.backoff.clone(),
                self.db_factory.clone(),
            ));
        }

        self.db_cli
            .execute(
                &self.db_factory.process_pending_events_3,
                &[&top, &"events"],
            )
            .await
            .unwrap();

        let time4 = SystemTime::now()
            .duration_since(start_time.clone())
            .unwrap()
            .as_millis();

        println!(
            "{:<16} | {}ms {}ms {}ms {}ms",
            "process_events".cyan().bold(),
            time1,
            time2,
            time3,
            time4
        );

        if top < last_event_poke as i32 {
            return false;
        }

        return true;
    }

    async fn send_transactions_for_destination_static(
        destination: String,
        data: Arc<RwLock<TransactionQueueData>>,
        fed_cli: Arc<FederationClient>,
        db_cli: Arc<tokio_postgres::Client>,
        backoff: Arc<Backoff>,
        db_factory: Arc<DBStatementsFactory>,
    ) {
        {
            let is_pending_opt = data
                .read()
                .await
                .dest_ongoing_transactions
                .get(destination.as_str()).cloned();

            if let Some(is_pending) = is_pending_opt {
                if is_pending {
                    return;
                }
            } else {
                data.write()
                    .await
                    .dest_ongoing_transactions
                    .insert(destination.clone(), true);
            }
        }

        let mut next_transaction_opt: Option<AtomicTransaction> = None;

        let mut retry = false;

        loop {
            if !retry {
                let mut data = data.write().await;

                next_transaction_opt = None;

                if let Some(transactions) =
                data.dest_pending_transactions.get_mut(destination.as_str())
                {
                    if !transactions.is_empty() {
                        let atomic_transaction = transactions.remove(0);

                        next_transaction_opt = Some(atomic_transaction);
                    }
                }

                if next_transaction_opt.is_none() {
                    data
                        .dest_ongoing_transactions
                        .remove(&destination);

                    return;
                }
            }

            retry = false;

            if let Some(next_atomic_transaction) = &next_transaction_opt {
                let next_transaction = next_atomic_transaction.read().await.clone();

                match fed_cli.send_transaction(&next_transaction).await {
                    Ok(_) => {
                        println!(
                            "{:<16} | transaction {} to {} completed",
                            "send_tran".dimmed(),
                            next_transaction.transaction_id,
                            destination,
                        );
                    }
                    Err(e) => {
                        let (is_down, backoff_for) =
                            backoff.set_backoff(destination.clone(), e).await;

                        if is_down {
                            println!(
                                "{:<16} | {} is DOWN",
                                "send_tran".cyan().bold(),
                                destination,
                            );

                            if data
                                .read()
                                .await
                                .dest_pending_transactions
                                .contains_key(destination.as_str())
                            {
                                data.write()
                                    .await
                                    .dest_pending_transactions
                                    .remove(destination.as_str());
                            }

                            return;
                        }

                        if backoff_for.as_secs() > 0 {
                            println!(
                                "{:<16} | {} is deferred sending transaction for {} sec",
                                "send_tran".cyan().bold(),
                                destination,
                                backoff_for.as_secs()
                            );

                            tokio::time::delay_for(backoff_for).await;

                            retry = true;

                            println!(
                                "{:<16} | {} is retrying",
                                "send_tran".cyan().bold(),
                                destination
                            );

                            continue;
                        }
                    }
                }

                if backoff.clear_backoff(destination.clone()).await {
                    println!(
                        "{:<16} | {} is back ON",
                        "send_tran".cyan().bold(),
                        destination,
                    );
                }

                {
                    let data = data.clone();
                    let db_cli = db_cli.clone();
                    let db_factory = db_factory.clone();

                    tokio::spawn(async move {
                        Self::clear_device_messages(&next_transaction, data, db_cli, db_factory)
                            .await;
                    });
                }
            }
        }
    }

    async fn transaction_for_destination(&self, destination: String) -> AtomicTransaction {
        {
            if !self
                .data
                .read()
                .await
                .dest_pending_transactions
                .contains_key(destination.as_str())
            {
                self.data
                    .write()
                    .await
                    .dest_pending_transactions
                    .insert(destination.clone(), Vec::new());
            }
        }

        {
            let mut data = self.data.read().await;

            if let Some(list) = data.dest_pending_transactions.get(destination.as_str()) {
                if !list.is_empty() {
                    let atomic_value = list.last().unwrap().clone();
                    let value = atomic_value.read().await;
                    if value.pdus.len() < Self::MAX_PDUS_PER_TRANSACTION
                        && value.edus.len() < Self::MAX_EDUS_PER_TRANSACTION
                    {
                        return atomic_value.clone();
                    }
                }
            }
        }

        self.data.write().await.txn_id += 1;

        let transaction = Transaction {
            transaction_id: (self.data.read().await.txn_id + 1).to_string(),
            destination: destination.clone(),
            origin: self.server_name.clone(),
            origin_server_ts: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_millis() as u64,
            previous_ids: Vec::new(),
            edus: Vec::new(),
            pdus: Vec::new(),
        };

        let atomic_transaction = Arc::new(RwLock::new(transaction));

        {
            let mut data = self.data.write().await;

            if let Some(mut list) = data.dest_pending_transactions.get_mut(destination.as_str()) {
                list.push(atomic_transaction.clone());
            }
        }

        return atomic_transaction;
    }

    async fn presence_destinations(
        &self,
        presence_vec: Vec<PresenceState>,
    ) -> Vec<(HashSet<String>, PresenceState)> {
        let mut result: Vec<(HashSet<String>, PresenceState)> = Vec::new();

        for presence in presence_vec.iter() {
            let mut hosts: HashSet<String> = HashSet::new();

            let rooms = self
                .room_cache_holder
                .rooms_for_user(presence.user_id.clone())
                .await;

            for room in rooms.iter() {
                if let Some(max_hosts_for_presence) = self.max_hosts_for_presence {
                    if room.hosts.len() > max_hosts_for_presence {
                        continue;
                    }
                }

                for host in room.hosts.iter() {
                    if self.backoff.host_is_down(host.clone()).await {
                        continue;
                    }

                    hosts.insert(host.clone());
                }
            }

            hosts.remove(self.server_name.as_str());

            result.push((hosts, presence.clone()));
        }

        result
    }
}
