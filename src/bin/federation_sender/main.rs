extern crate cortex;

mod backoff;
mod device_content_set;
mod error;
mod federation_client;
mod signing_key;
mod transaction;
mod transaction_queue;

use std::collections::HashMap;
use std::sync::Arc;

use tokio::fs::File;
use tokio::prelude::*;

use failure::Error;

use clap::{App, Arg};

use futures::{FutureExt, StreamExt};

use colored::*;

use signing_key::SigningKey;

use cortex::config::Config;

use synrep::commands::{Commands, FederationAckCommand, RDataCommand};
use synrep::connection::{Connection, ReplicationLine, RowsData};
use synrep::streams::{EventStreamRow, FederationStreamRow, StreamRow, StreamRows};

use crate::transaction_queue::TransactionQueue;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let matches = App::new("Cortex")
        .version("0.0.0")
        .author("Black Hat <bhat@encom.eu.org>")
        .about("A dev worker")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Path to config file")
                .default_value("appsettings.yaml")
                .takes_value(true),
        )
        .get_matches();

    let config_path = matches.value_of("config").unwrap();

    let config = Config::from_file(config_path)?;

    // Output worker information
    println!("{:<16} | {}", "Cortex".yellow().bold(), "Federation sender");
    println!("{:<16} | {}", "Config path".cyan().bold(), config_path);
    println!(
        "{:<16} | {}:{}",
        "Database".cyan().bold(),
        config.database.host,
        config.database.port
    );
    println!(
        "{:<16} | {}",
        "Synapse".cyan().bold(),
        config.synapse.server_name
    );
    println!(
        "{:<16} | {}:{}",
        "Replication".cyan().bold(),
        config.synapse.host,
        config.synapse.port
    );
    println!(
        "{:<16} | {}",
        "Signing Key".cyan().bold(),
        config.synapse.signing_key_path
    );

    // Load signing key from filesystem
    let mut signing_key_file = File::open(config.synapse.signing_key_path).await?;
    let mut raw_signing_key = String::new();
    signing_key_file
        .read_to_string(&mut raw_signing_key)
        .await?;

    let mut db_config = tokio_postgres::Config::new();
    db_config.host(config.database.host.as_str());
    db_config.port(config.database.port);
    db_config.dbname(config.database.database.as_str());
    db_config.user(config.database.user.as_str());
    db_config.password(config.database.password.as_str());

    let (db_cli, connection) = db_config.connect(tokio_postgres::NoTls).await?;

    let connection = connection.map(|r| {
        if let Err(e) = r {
            eprintln!("{} {}", "Connection error:".yellow().bold(), e.to_string());
        }
    });
    tokio::spawn(connection);

    let db_cli = Arc::new(db_cli);

    let transaction_queue = TransactionQueue::new(
        config.synapse.server_name.clone(),
        db_cli.clone(),
        SigningKey::from_raw(raw_signing_key)?,
        config.worker.max_concurrency,
        config.worker.max_hosts_for_presence,
        config.worker.max_room_cache,
    )
        .await;

    let atomic_transaction_queue = Arc::new(transaction_queue);

    let (mut conn, background) = Connection::new(
        config.name,
        config.synapse.server_name,
        config.synapse.host,
        config.synapse.port,
    )
        .await?;
    tokio::spawn(background);

    println!("{}", "Connected to replication".green().bold());

    conn.subscribe(EventStreamRow::NAME.to_string(), 0).await;
    conn.subscribe(FederationStreamRow::NAME.to_string(), 0)
        .await;

    let mut fed_stream_position: Option<u64> = {
        let stmt1 = db_cli
            .prepare("SELECT stream_id FROM federation_stream_position WHERE type = $1")
            .await
            .unwrap();

        let rows: Vec<tokio_postgres::Row> = db_cli.query(&stmt1, &[&"federation"]).await.unwrap();

        if !rows.is_empty() {
            let row = &rows[0];

            let stream_id = row.get::<usize, i32>(0) as u64; // why is it psql::int4...

            Some(stream_id)
        } else {
            None
        }
    };

    let mut last_ack: Option<u64> = None;

    while let Some(result) = conn.next().await {
        match result {
            Ok(command) => {
                //                println!("{}: {:?}", "Recv update".blue().bold(), command);

                match command {
                    ReplicationLine::RData(replication_data) => {
                        //                        println!(
                        //                            "{:<16} | {} {}",
                        //                            "RData token".cyan(),
                        //                            replication_data.stream,
                        //                            replication_data.token
                        //                        );

                        if replication_data.stream == "federation" {
                            fed_stream_position = Some(replication_data.token);
                        }

                        match replication_data.rows_data {
                            RowsData::Event(events) => {
                                let atomic_transaction_queue = atomic_transaction_queue.clone();
                                let token = replication_data.token.clone();

                                tokio::spawn(async move {
                                    atomic_transaction_queue.set_event_position(token).await;
                                });
                            }
                            RowsData::Federation(federations) => {
                                for federation in federations {
                                    match federation {
                                        FederationStreamRow::Presence(presence_row) => {
                                            if !config.synapse.presence {
                                                continue;
                                            }

                                            let atomic_transaction_queue =
                                                atomic_transaction_queue.clone();
                                            let presence_vec = presence_row.presence.clone();

                                            tokio::spawn(async move {
                                                atomic_transaction_queue
                                                    .send_presence(presence_vec)
                                                    .await;
                                            });
                                        }
                                        FederationStreamRow::Edu(edu_row) => {
                                            for edu in edu_row.edus {
                                                let atomic_transaction_queue =
                                                    atomic_transaction_queue.clone();

                                                tokio::spawn(async move {
                                                    atomic_transaction_queue.send_edu(edu).await;
                                                });
                                            }
                                        }
                                        FederationStreamRow::Keyed(keyed_edu_row) => {
                                            for (keys, keyed_edu) in keyed_edu_row.keyed_edus {
                                                let key = keys.join(":");

                                                let atomic_transaction_queue =
                                                    atomic_transaction_queue.clone();

                                                tokio::spawn(async move {
                                                    atomic_transaction_queue
                                                        .send_keyed_edu(keyed_edu, key)
                                                        .await;
                                                });
                                            }
                                        }
                                        FederationStreamRow::Device(device_row) => {
                                            if !device_row.devices.is_empty() {
                                                println!(
                                                    "{:<16} | prodded device updates for {:?}",
                                                    "Fed Device".cyan(),
                                                    device_row.devices,
                                                );
                                            }

                                            for device in device_row.devices {
                                                let atomic_transaction_queue =
                                                    atomic_transaction_queue.clone();

                                                tokio::spawn(async move {
                                                    atomic_transaction_queue
                                                        .send_device_messages(device)
                                                        .await;
                                                });

                                                if let Some(fsp) = fed_stream_position {
                                                    if last_ack.unwrap_or(0) >= fsp {
                                                        continue;
                                                    }

                                                    conn.send_command(FederationAckCommand {
                                                        token: fsp,
                                                    })
                                                        .await;
                                                }
                                            }
                                        }
                                        _ => {}
                                    }
                                }
                            }
                            _ => {}
                        }
                    }
                    ReplicationLine::Position(position) => {
                        println!(
                            "{:<16} | {} {}",
                            "Position".cyan(),
                            position.stream,
                            position.token
                        );

                        if position.stream == "federation" {
                            fed_stream_position = Some(position.token);
                        }

                        if position.stream == "events" {
                            let atomic_transaction_queue = atomic_transaction_queue.clone();
                            let token = position.token.clone();

                            tokio::spawn(async move {
                                atomic_transaction_queue.set_event_position(token).await;
                            });
                        }
                    },
                    ReplicationLine::RemoteUp(remote_server_up) => {
                        println!(
                            "{:<16} | {} is up",
                            "RemoteUp".cyan(),
                            remote_server_up.server_name
                        );

                        atomic_transaction_queue.clear_backoff(remote_server_up.server_name).await;
                    },
                    _ => {}
                }
            }
            Err(e) => {
                println!("{}", e);
            }
        }
    }

    Ok(())
}
