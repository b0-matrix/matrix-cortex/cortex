use std::error;
use std::fmt;

use serde_json::Value;

use reqwest::StatusCode;

#[derive(Debug, Clone)]
pub enum FederationError {
    Busy(u16, Option<u64>),
    Connection,
    Serde,
    Transaction(TransactionFailureError),
    Timeout,
}

impl error::Error for FederationError {
    fn description(&self) -> &str {
        match *self {
            FederationError::Busy(code, backoff) => "remote server busy",
            FederationError::Connection => "connection error",
            FederationError::Serde => "serialize/deserialize error",
            FederationError::Transaction(ref e) => "transaction failure",
            FederationError::Timeout => "remote server timeout",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            FederationError::Busy(_, _) => None,
            FederationError::Connection => None,
            FederationError::Serde => None,
            FederationError::Transaction(_) => None,
            FederationError::Timeout => None,
        }
    }
}

impl fmt::Display for FederationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            FederationError::Busy(code, backoff) => write!(f, "remote server busy: {}", code),
            FederationError::Connection => write!(f, "connection error"),
            FederationError::Serde => write!(f, "serialize/deserialize error"),
            // This is a wrapper, so defer to the underlying types' implementation of `fmt`.
            FederationError::Transaction(ref e) => write!(f, "transaction error"),
            FederationError::Timeout => write!(f, "remote server timeout"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct TransactionFailureError {
    pub code: StatusCode,
    pub error: String,
    pub error_code: String,
    pub host: String,
}

impl TransactionFailureError {
    pub fn new(host: String, code: StatusCode, resp: Value) -> TransactionFailureError {
        TransactionFailureError {
            host,
            code,
            error: resp
                .get("error")
                .unwrap_or(&Value::Null)
                .as_str()
                .unwrap_or("")
                .to_string(),
            error_code: resp
                .get("errcode")
                .unwrap_or(&Value::Null)
                .as_str()
                .unwrap_or("")
                .to_string(),
        }
    }
}

impl fmt::Display for TransactionFailureError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.error)
    }
}

impl From<TransactionFailureError> for FederationError {
    fn from(err: TransactionFailureError) -> FederationError {
        FederationError::Transaction(err)
    }
}
