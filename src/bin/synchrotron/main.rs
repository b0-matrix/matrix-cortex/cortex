extern crate cortex;

use clap::{App, Arg};

use cortex::config::Config;

use synrep::connection::Connection;
use synrep::streams::EventStreamRow;

fn main() {
    let matches = App::new("Cortex")
        .version("0.0.0")
        .author("Black Hat <bhat@encom.eu.org>")
        .about("A synchrotron worker")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Path to config file")
                .default_value("appsettings.yaml")
                .takes_value(true),
        )
        .get_matches();
}
