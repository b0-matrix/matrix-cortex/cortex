use serde_json::Value;

#[derive(Debug, Clone)]
pub struct Event {
    pub event_id: String,
    pub event_type: String,
    pub room_id: String,
    pub sender_id: String,
    pub content: Value,
    pub format_version: u32,
}
