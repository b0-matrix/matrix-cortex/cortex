pub mod model;
pub mod resolver;

#[cfg(test)]
mod tests {
    use crate::matrix::resolver::{
        HostRecord, HostResolver, SRVResolver, SRVResolverError, ServerName, WellKnownResolver,
        WellKnownResolverError,
    };
    use std::net::Ipv4Addr;

    #[tokio::test]
    async fn test_well_known_resolver() {
        let resolver = WellKnownResolver::new();

        match resolver.resolve("matrix.org".to_string()).await {
            Ok(result1) => {
                panic!("matrix.org does not have well-known");
            }
            Err(e) => match e {
                WellKnownResolverError::ServerUnavailable => {
                    panic!("matrix.org does not support well-known");
                }
                _ => {}
            },
        }

        match resolver.resolve("encom.eu.org".to_string()).await {
            Ok(result2) => {
                assert_eq!(
                    result2.hostname,
                    url::Host::Domain("matrix.encom.eu.org".to_string())
                );
                assert_eq!(result2.port, Some(443));
            }
            Err(e) => {
                panic!("encom.eu.org does have well-known");
            }
        }
    }

    #[tokio::test]
    async fn test_srv_resolver() {
        let resolver = SRVResolver::new();

        match resolver.resolve("matrix.org".to_string()).await {
            Ok(result1) => {
                assert_eq!(
                    result1.hostname,
                    url::Host::Domain(
                        "matrix-federation.matrix.org.cdn.cloudflare.net".to_string()
                    )
                );
                assert_eq!(result1.port, Some(8443));
            }
            Err(e) => {
                panic!("matrix.org does have SRV");
            }
        }

        match resolver.resolve("encom.eu.org".to_string()).await {
            Ok(result2) => {
                panic!("encom.eu.org does not have SRV");
            }
            Err(e) => {}
        }
    }

    #[tokio::test]
    async fn test_resolver() {
        let (mut resolver, background) = HostResolver::new(8448).await;

        tokio::spawn(background);

        let result1 = resolver.host_record("matrix.org".to_string()).await;

        assert_eq!(
            result1.resolved,
            ServerName {
                hostname: url::Host::Domain(
                    "matrix-federation.matrix.org.cdn.cloudflare.net".to_string()
                ),
                port: Some(8443),
            }
        );

        let result2 = resolver.host_record("example.org".to_string()).await;

        assert_eq!(
            result2.resolved,
            ServerName {
                hostname: url::Host::Domain("example.org".to_string()),
                port: Some(8448),
            }
        );

        let result3 = resolver.host_record("encom.eu.org".to_string()).await;

        assert_eq!(
            result3.resolved,
            ServerName {
                hostname: url::Host::Domain("matrix.encom.eu.org".to_string()),
                port: Some(443),
            }
        );

        let result4 = resolver.host_record("192.168.1.1".to_string()).await;

        assert_eq!(
            result4.resolved,
            ServerName {
                hostname: url::Host::Ipv4(Ipv4Addr::new(192, 168, 1, 1)),
                port: Some(8448),
            }
        );

        let result5 = resolver.host_record("example.org:5555".to_string()).await;

        assert_eq!(
            result5.resolved,
            ServerName {
                hostname: url::Host::Domain("example.org".to_string()),
                port: Some(5555),
            }
        );
    }
}
