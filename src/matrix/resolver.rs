use std::cmp::PartialEq;
use std::collections::{HashMap, HashSet};
use std::fmt;
use std::future::Future;
use std::io;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str::FromStr;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use tokio::runtime::Handle;
use tokio::sync::RwLock;

use futures::future::TryFutureExt;

use url::Url;

use colored::*;

use serde_json::Value;

use trust_dns_resolver::config::*;
use trust_dns_resolver::proto::rr::rdata::SRV;
use trust_dns_resolver::TokioAsyncResolver;

#[derive(Clone, Debug, PartialEq)]
pub struct HostRecord {
    // 80 bytes
    pub host: String,
    pub resolved: ServerName,
    pub last_access_time: u64,
    pub srv: bool,
    pub is_down: bool,
}

impl HostRecord {
    const TTL: Duration = Duration::from_secs(3600);
    const TTL_RETRY: Duration = Duration::from_secs(600);

    pub fn new(host: String, resolved: ServerName, srv: bool, is_down: bool) -> Self {
        HostRecord {
            host,
            resolved,
            last_access_time: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
            srv,
            is_down,
        }
    }

    pub fn expired(&self) -> bool {
        let ttl = if self.is_down {
            Self::TTL_RETRY
        } else {
            Self::TTL
        };

        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs()
            > self.last_access_time + ttl.as_secs()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct ServerName {
    pub hostname: url::Host<String>,
    pub port: Option<u16>,
}

impl fmt::Display for ServerName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.port {
            Some(p) => write!(f, "{}:{}", self.hostname, p),
            None => write!(f, "{}", self.hostname),
        }
    }
}

impl ServerName {
    pub fn parse(server_name: String) -> Result<Self, url::ParseError> {
        let mut parsed_url = url::Url::parse(format!("https://{}", server_name).as_str())?;

        let mut port = None;

        if parsed_url.port().is_some() {
            // Might be no port or port is 443.
            port = parsed_url.port();
        } else if server_name.ends_with(":443") {
            port = Some(443);
        }

        let hostname = parsed_url.host().unwrap().to_owned();

        Ok(Self { hostname, port })
    }
}

type AtomicHostRecord = Arc<RwLock<Option<HostRecord>>>;

pub struct HostResolver {
    hosts: Arc<RwLock<HashMap<String, AtomicHostRecord>>>,
    default_port: u16,
    srv_resolver: Arc<SRVResolver>,
    well_known_resolver: Arc<WellKnownResolver>,

    server_validator: Arc<ServerValidator>,

    is_running: Arc<AtomicBool>,
}

impl HostResolver {
    pub async fn new(default_port: u16) -> (Self, impl Future<Output=()>) {
        let hosts = Arc::new(RwLock::new(HashMap::new()));
        let is_running = Arc::new(AtomicBool::new(true));

        let resolver = HostResolver {
            default_port,
            hosts: hosts.clone(),
            srv_resolver: Arc::new(SRVResolver::new().await),
            well_known_resolver: Arc::new(WellKnownResolver::new()),

            server_validator: Arc::new(ServerValidator::new()),

            is_running: is_running.clone(),
        };

        let background = async move {
            while is_running.load(Ordering::SeqCst) {
                tokio::time::delay_for(Duration::from_secs(600)).await;

                let mut hosts = hosts.write().await;

                let mut hosts_expired: Vec<String> = Vec::new();

                for (hostname, record) in hosts.iter() {
                    let record = record.read().await;

                    if let Some(r) = record.clone() {
                        if r.expired() {
                            hosts_expired.push(hostname.clone());
                        }
                    }
                }

                for hostname in hosts_expired {
                    hosts.remove(&hostname);
                }
            }
        };

        (resolver, background)
    }

    pub async fn host_record(&self, server_name: String) -> HostRecord {
        // destination might be an IP address or DNS style hostname.
        let atomic_host = {
            let atomic_host_opt = self.hosts.read().await.get(server_name.as_str()).cloned();

            if let Some(atomic_host) = atomic_host_opt {
                atomic_host
            } else {
                let atomic_host = Arc::new(RwLock::new(None));

                self.hosts
                    .write()
                    .await
                    .insert(server_name.clone(), atomic_host.clone());

                atomic_host
            }
        };

        let host = atomic_host.read().await.clone();

        if let Some(h) = host {
            return h;
        }

        let mut atomic_host = atomic_host.write().await;

        let (mut record, srv) = self
            .resolve_with_default_port(server_name.clone(), self.default_port)
            .await;

        if !self.server_validator.validate(&record).await {
            record.is_down = true;
        }

        *atomic_host = Some(record.clone());

        record
    }

    pub async fn clear_record(&self, server_name: String) -> bool {
        return self.hosts.write().await.remove(&server_name).is_some();
    }

    async fn resolve_with_default_port_static(
        server_name: String,
        default_port: u16,
        well_known_resolver: Arc<WellKnownResolver>,
        srv_resolver: Arc<SRVResolver>,
    ) -> (HostRecord, bool) {
        let (mut result, srv) =
            Self::resolve_static(server_name.clone(), well_known_resolver, srv_resolver).await;

        if result.port.is_none() {
            result.port = Some(default_port);
        }

        let record = HostRecord::new(server_name, result, srv, false);

        (record, srv)
    }

    pub(crate) async fn resolve_with_default_port(
        &self,
        server_name: String,
        default_port: u16,
    ) -> (HostRecord, bool) {
        Self::resolve_with_default_port_static(
            server_name,
            default_port,
            self.well_known_resolver.clone(),
            self.srv_resolver.clone(),
        )
            .await
    }

    async fn resolve_static(
        server_name: String,
        well_known_resolver: Arc<WellKnownResolver>,
        srv_resolver: Arc<SRVResolver>,
    ) -> (ServerName, bool) {
        if let Some(s) = Self::handle_basic_host(server_name.clone()) {
            return (s, false);
        }

        let well_known_result = well_known_resolver.resolve(server_name.clone()).await;

        if let Ok(s) = well_known_result {
            return (s, false);
        }

        let srv_result = srv_resolver.resolve(server_name.clone()).await;

        if let Ok(s) = srv_result {
            return (s, true);
        }

        let primitive_server_name = ServerName::parse(server_name).unwrap();

        (primitive_server_name, false)
    }

    pub(crate) async fn resolve(&self, server_name: String) -> (ServerName, bool) {
        Self::resolve_static(
            server_name,
            self.well_known_resolver.clone(),
            self.srv_resolver.clone(),
        )
            .await
    }

    fn handle_basic_host(server_name: String) -> Option<ServerName> {
        let parsed_server_name = match ServerName::parse(server_name) {
            Ok(p) => p,
            Err(e) => {
                return None;
            }
        };

        match parsed_server_name.hostname {
            url::Host::Ipv4(_) => {
                return Some(parsed_server_name);
            }
            url::Host::Ipv6(_) => {
                return Some(parsed_server_name);
            }
            url::Host::Domain(_) => {
                if parsed_server_name.port.is_some() {
                    return Some(parsed_server_name);
                }
            }
        }

        None
    }

    fn create_host_url(&self, destination: String) -> Url {
        let mut unprocessed_uri = Url::parse(format!("https://{}", destination).as_str()).unwrap();
        println!("{} {}", destination, unprocessed_uri);
        if unprocessed_uri.port().is_none() {
            // Might be no port or port is 443.
            if !destination.ends_with(":443") {
                // HTTPS default port numbers are not reflected in the serialization.
                unprocessed_uri.set_port(Some(self.default_port)).unwrap();
            }
        }

        unprocessed_uri
    }
}

impl Drop for HostResolver {
    fn drop(&mut self) {
        self.is_running.store(false, Ordering::SeqCst);
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum WellKnownResolverError {
    ServerUnavailable,
    ServerUnacceptable,
}

pub struct WellKnownResolver {
    max_retry: u8,
}

impl WellKnownResolver {
    pub fn new() -> Self {
        Self { max_retry: 3 }
    }

    pub async fn resolve(&self, server_name: String) -> Result<ServerName, WellKnownResolverError> {
        let req_url =
            Url::parse(format!("https://{}/.well-known/matrix/server", server_name).as_str())
                .map_err(|_| WellKnownResolverError::ServerUnacceptable)?;

        for i in 0..self.max_retry {
            let result = reqwest::Client::builder()
                .tcp_nodelay()
                .timeout(Duration::from_secs(10))
                .build()
                .unwrap()
                .get(req_url.clone())
                .send()
                .and_then(|r| async move { r.text().await })
                .await;

            match result {
                Ok(body) => {
                    let v: Value = serde_json::from_str(body.as_str())
                        .map_err(|_| WellKnownResolverError::ServerUnacceptable)?;
                    if let Some(well_known_host_value) = v.get("m.server") {
                        let well_known_host = well_known_host_value.as_str().unwrap().to_string();
                        let server_name = ServerName::parse(well_known_host)
                            .map_err(|_| WellKnownResolverError::ServerUnacceptable)?;
                        return Ok(server_name);
                    }

                    // Parsing error or irrelevant page, exit
                    return Err(WellKnownResolverError::ServerUnacceptable);
                }
                Err(e) => {
                    if e.is_timeout() {
                        return Err(WellKnownResolverError::ServerUnavailable);
                    }

                    if e.is_status() {
                        let status_code = e.status().unwrap();

                        if status_code.is_server_error() {
                            println!(
                                "{} {}",
                                "Server status error, retrying:",
                                status_code.as_u16()
                            );

                            tokio::time::delay_for(Duration::from_secs(1)).await;

                            continue;
                        }

                        // Else the server does not have well-known, exit
                        return Err(WellKnownResolverError::ServerUnacceptable);
                    }

                    // Redirect. The server has wrong setup, exit
                    if e.is_redirect() {
                        return Err(WellKnownResolverError::ServerUnacceptable);
                    }
                }
            }
        }

        Err(WellKnownResolverError::ServerUnavailable)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum SRVResolverError {
    DNS,
}

pub struct SRVResolver {
    resolver: TokioAsyncResolver,
}

impl SRVResolver {
    pub async fn new() -> Self {
        let mut resolver_opts = ResolverOpts::default();
        resolver_opts.timeout = Duration::from_secs(10);

        let resolver_config = ResolverConfig::default();

        let resolver = TokioAsyncResolver::tokio(resolver_config, resolver_opts)
            .await
            .unwrap();

        Self { resolver }
    }

    pub async fn resolve(&self, server_name: String) -> Result<ServerName, SRVResolverError> {
        let service_name = format!("_matrix._tcp.{}.", server_name);

        let result = self.resolver.srv_lookup(service_name).await;

        if let Ok(response) = result {
            let srv_records: Vec<SRV> = response.iter().map(|s| s.clone()).collect();

            if !srv_records.is_empty() {
                let srv_record = srv_records.get(0).unwrap();
                let mut target = srv_record.target().to_string();
                if srv_record.target().is_fqdn() {
                    target.pop();
                }

                return Ok(ServerName {
                    hostname: url::Host::Domain(target),
                    port: Some(srv_record.port()),
                });
            }
        }

        Err(SRVResolverError::DNS)
    }
}

pub struct ServerValidator {
    max_retry: u8,
}

impl ServerValidator {
    pub fn new() -> Self {
        Self { max_retry: 3 }
    }

    pub async fn validate(&self, record: &HostRecord) -> bool {
        let mut request_url =
            url::Url::parse(format!("https://{}", record.resolved.hostname).as_str()).unwrap();

        request_url.set_scheme("https").unwrap();
        request_url
            .set_host(Some(record.resolved.hostname.to_string().as_str()))
            .unwrap();
        request_url.set_port(record.resolved.port.clone()).unwrap();
        request_url.set_path("/_matrix/federation/v1/version");

        for i in 0..self.max_retry {
            let result = reqwest::Client::builder()
                .tcp_nodelay()
                .timeout(Duration::from_secs(10))
                .danger_accept_invalid_certs(true)
                .build()
                .unwrap()
                .get(request_url.clone())
                .send()
                .await;

            match result {
                Ok(res) => {
                    let status_code = res.status().as_u16();

                    if status_code == 403 {
                        // Quick fix for matrix.org
                        return true;
                    }

                    if let Ok(resp_json) = res.json().await {
                        let resp_json: serde_json::Value = resp_json;

                        if let Some(server) = resp_json.get("server") {
                            if let Some(version) = server.get("version") {
                                return true;
                            }
                        }
                    }
                }
                Err(e) => {
                    return false;
                }
            }
        }

        return false;
    }
}
