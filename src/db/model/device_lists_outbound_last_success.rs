use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeviceListsOutboundLastSuccess {
    pub destination: String,
    pub user_id: String,
    pub stream_id: u64,
}
