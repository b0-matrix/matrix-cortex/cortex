use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct E2EDeviceKeysJson {
    pub user_id: String,
    pub device_id: String,
    pub ts_added_ms: u64,
    pub key_json: String,
}
