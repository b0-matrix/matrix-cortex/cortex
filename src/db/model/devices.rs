use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Devices {
    pub user_id: String,
    pub device_id: String,
    pub display_name: Option<String>,
}
