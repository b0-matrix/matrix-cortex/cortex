# Cortex

A group of workers for offloading your Synapse pressure.

# Configuration

The file appsettings.default.yaml is the configuration template. 
Most of the properties are self-explanatory.

# Usage

Make sure you have some decent knowledge about running synapse workers, 
and have succeeded in running synapse workers before you start.

## Docker Image

All cortex workers are packaged as a Docker image.

Example docker-compose.yaml:

```yaml
version: "2.4"

services:
  federation_sender:
    image: black0/cortex:latest
    restart: always
    command: "/app/federation_sender --config /data/federation_sender.yaml"
    volumes:
      - /data/workers:/data
```

# Building

```bash
cargo build --all
```
